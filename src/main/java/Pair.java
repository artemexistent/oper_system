import lombok.Getter;

public class Pair {
  Integer first;
  @Getter
  Integer second;

  public Pair(Integer first, Integer second) {
    this.first = first;
    this.second = second;
  }
}
