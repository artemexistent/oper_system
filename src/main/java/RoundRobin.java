import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

public class RoundRobin {
  static void startAndGetAvgTime(List<Integer> processes, List<Integer> burstTime, int quantum, int size) {
    List<Pair> processesAdnBurstTime = new ArrayList<>();
    for (int i = 0; i < size; i++) {
      processesAdnBurstTime.add(new Pair(processes.get(i), burstTime.get(i)));
    }
    List<Integer> waitTime = getWaitTime(processesAdnBurstTime, quantum, burstTime);
    List<Integer> turnAroundTime = new ArrayList<>();
    int totalTurnTime = 0;
    int totalWaitTime = 0;
    for (int i = 0; i < size; i++) {
      turnAroundTime.add(burstTime.get(i) + waitTime.get(i));
      totalTurnTime += turnAroundTime.get(i);
      totalWaitTime += waitTime.get(i);
    }
    print(processes, burstTime, quantum, waitTime, turnAroundTime, totalWaitTime, totalTurnTime);
  }

  private static void print(List<Integer> processes, List<Integer> burstTime, int quantum, List<Integer> waitTime, List<Integer> turnAroundTime, int totalWaitTime, int totalTurnTime) {
    System.out.println("Quantum = " + quantum);
    System.out.println("Pr\tBT\tWT\tTA");
    for (int i = 0; i < processes.size(); i++) {
      System.out.printf("%d\t%d\t%d\t%d\n", processes.get(i), burstTime.get(i), waitTime.get(i), turnAroundTime.get(i));
    }
    System.out.println("Average waiting time = " + (float) totalWaitTime / (float) processes.size());
    System.out.println("nAverage turn around time = " + (float) totalTurnTime / (float) processes.size());
  }

  private static List<Integer> getWaitTime(List<Pair> processes, int quantum, List<Integer> burstTime) {
    Queue<Integer> queue = new ConcurrentLinkedQueue<>();
    List<Pair> pairs = new ArrayList<>(processes);
    List<Integer> waitTime = new ArrayList<>();
    for (int i = 0; i < pairs.size(); i++) {
      waitTime.add(null);
    }
    int currentTime = 0;
    int time = 0;
    int finishedProcess = 0;
    Integer processId = null;
    while (finishedProcess < processes.size()) {
      for (int i = 0; i < pairs.size(); i++) {
        if (pairs.get(i).first == currentTime && !queue.contains(i) && (processId == null || processId != i) && pairs.get(i).second != 0) {
          queue.add(i);
        }
      }
      if (processId == null) {
        processId = queue.poll();
      } else {
        if (pairs.get(processId).second == 0) {
          waitTime.set(processId, (currentTime - burstTime.get(processId) - processes.get(processId).first));
          finishedProcess++;
          processId = null;
          time = 0;
          continue;
        }
        if (time == quantum) {
          queue.add(processId);
          processId = null;
          time = 0;
          continue;
        }
      }
      time++;
      currentTime++;
      pairs.get(processId).second--;
    }
    return waitTime;
  }

  public static void main(String[] args) {
    List<Integer> processes = List.of(0, 5, 1, 6, 8);
    List<Integer> burstTime = List.of(8, 2, 7, 3, 5);
//    List<Integer> processes = List.of(0);
//    List<Integer> burstTime = List.of(8);
    startAndGetAvgTime(processes, burstTime, 3, processes.size());
  }

}


